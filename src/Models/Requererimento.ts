/**
 * Requerimento do aluno.
 */
export class Requerimento{    

    // id unico do requerimento
    id: string;
    // numero do requerimento
    numeroRequerimento: number;
    // texto especificando o requerimento
    texto: string;
    // status do requerimento, onde os tipos possíveis são: "PENDENTE", "ATENDIDO"
    status: string;

    public constructor(numeroRequerimento: number, texto: string, status: string){
        this.numeroRequerimento = numeroRequerimento;
        this.texto = texto;
        this.status = status;
    }
}