import { Usuario } from "../Models/Usuario";
import { SourceMapGenerator } from "@angular/compiler/src/output/source_map";

export class Aluno extends Usuario{

    // número da matricula
    matricula: string;
    // curso
    curso: string;

    public constructor(email: string, nome: string, senha: string, matricula: string, curso: string){
        super(email,nome,senha);
        this.matricula = matricula;
        this.curso = curso;
    }

    
}