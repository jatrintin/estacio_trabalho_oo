import { Requerimento } from "./Requererimento";
import { Aluno } from "./Aluno";

/**
 * Requerimentos
 */
export class Requerimentos {

    // Aluno que faz o requerimento
    aluno: Aluno;
    // itens de requerimento
    items: Requerimento[] = [];


    public adicionarRequerimento(item: Requerimento){
        this.items.push(item);
    }

    public getRequerimentos(){
        return this.items;
    }

}