export class Usuario {

    // id unico do usuario
    id: string;
    // email do usuário
    email: string;
    // nome do usuário
    nome: string;
    // senha do usuário
    senha: string;

    public constructor(email: string, nome: string, senha: string){
        this.email = email;
        this.nome = nome;
        this.senha = senha;
    }
  
}