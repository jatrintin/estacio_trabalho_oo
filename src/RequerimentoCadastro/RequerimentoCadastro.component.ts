import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, NgForm } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { Requerimento } from 'src/Models/Requererimento';
import { RequerimentosDAOService } from 'src/DAOS/RequerimentosDAO.service';
import { Aluno } from 'src/Models/Aluno';
import { SessionService } from 'src/app/Session.service';

@Component({
  selector: 'app-RequerimentoCadastro',
  templateUrl: './RequerimentoCadastro.component.html',
  styleUrls: ['./RequerimentoCadastro.component.css']
})
export class RequerimentoCadastroComponent implements OnInit {
  
  
  RequerimentoForm : FormGroup;
  txtCodigo: string; 
  aleatorioCodigo: number = 0;
  requerimento : Requerimento;

  constructor(private route: ActivatedRoute,private session: SessionService, private requerimentosDAOService: RequerimentosDAOService, private router: Router, private formBuilder: FormBuilder) { }

  ngOnInit() {

    // requerimento
    this.RequerimentoForm = this.formBuilder.group({
      'texto' : [null, Validators.required],
    });

    this.carregarTela();
  } 

  carregarTela(){

    // se tiver id, carrega na tela o requerimento
    let aluno = this.session.getAlunoLogado();
    const id = this.route.snapshot.paramMap.get('id');

    if (id){
      this.requerimentosDAOService.loadRequerimentoPorId(aluno,id)
      .then((req)=> {
        this.requerimento = req;
        this.txtCodigo = this.requerimento.numeroRequerimento + "";
        // carrega o texto
        this.RequerimentoForm.controls['texto'].setValue( this.requerimento.texto);
      });
    }
    else {
      this.aleatorioCodigo = Math.floor(Math.random() * 100) + 1;
      this.txtCodigo = this.aleatorioCodigo.toString();
    }

  }

  onFormSubmit(data:NgForm) {
    let aluno: Aluno = this.session.getAlunoLogado();

    // se tem o registro, atualiza
    if (this.requerimento && this.requerimento.id){

      this.requerimento.texto = data['texto'];

      this.requerimentosDAOService.updateRequerimento(aluno, this.requerimento)
      
      .then(()=> {
        alert("Requerimento Atualizado com sucesso!");
        this.router.navigate(['/requerimentos']);
      })
      .catch(()=>{
        alert("Erro ao atualizar requerimento. Por favor, tente mais tarde.");
      });

    }
    else
    // cria um novo
    {    
      this.requerimento = new Requerimento(this.aleatorioCodigo, data['texto'], "PENDENTE");
    
      this.requerimentosDAOService.createRequerimento(aluno, this.requerimento)
      
      .then(()=> {
        alert("Requerimento criado com sucesso! Em breve entraremos em contato!");
        this.router.navigate(['/requerimentos']);
      })
      .catch(()=>{
        alert("Erro ao criar requerimento. Por favor, tente mais tarde.");
      });
    }
  }

  clickExcluir(){
    let aluno: Aluno = this.session.getAlunoLogado();
    
    this.requerimentosDAOService.deleteRequerimento(aluno, this.requerimento)
    
    .then(()=> {
      alert("Requerimento excluído com sucesso!");
      this.router.navigate(['/requerimentos']);
    })
    .catch(()=>{
      alert("Erro ao excluir requerimento. Por favor, tente mais tarde.");
      this.router.navigate(['/requerimentos']);
    });
  }

}
