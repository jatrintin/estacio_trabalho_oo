import { Component, OnInit } from '@angular/core';
import { Aluno } from 'src/Models/Aluno';
import { Requerimentos } from 'src/Models/Requerimentos';
import { RequerimentosDAOService } from 'src/DAOS/RequerimentosDAO.service';
import { SessionService } from 'src/app/Session.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-Requerimentos',
  templateUrl: './Requerimentos.component.html',
  styleUrls: ['./Requerimentos.component.css']
})
export class RequerimentosComponent implements OnInit {

  requerimentos: Requerimentos;
  aluno: Aluno;

  constructor(private session: SessionService,private router: Router, private requerimentosService: RequerimentosDAOService){
  }

  ngOnInit() {
    this.aluno = this.session.getAlunoLogado();
    this.carregarRequerimentosDoAluno(this.aluno);
  }

  ngAfterViewInit() {
    
  }

  //** carrega pagina de requerimentos */
  carregarRequerimentosDoAluno(aluno: Aluno){

    this.requerimentosService.loadRequerimentosPorAluno(aluno)
    .subscribe((requerimentos)=>{
      this.requerimentos = requerimentos;
    });
  }

  selectRow(requerimento){
    this.router.navigate(['requerimento', requerimento.id]);
  }

}
