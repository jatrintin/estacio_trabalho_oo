import { Injectable } from '@angular/core';
import { Aluno } from 'src/Models/Aluno';
import { Observable } from 'rxjs';
import * as firebase from 'firebase/app';
import 'firebase/firestore';
import { Requerimento } from 'src/Models/Requererimento';
import { Requerimentos } from 'src/Models/Requerimentos';


/**
 * DAO de Requerimentos
 */
@Injectable()
export class RequerimentosDAOService {

    constructor() {        
    }

      /**
     * cria um requerimento
     * @param aluno 
     * @param texto 
     */
    public createRequerimento(aluno: Aluno, requerimento: Requerimento){

      // cria requerimento, repassando o id do aluno
      let table = firebase.firestore().collection("requerimentos").doc("aluno_" + aluno.id).collection("requerimentos");

      return new Promise( (resolve,reject)=>{

          // aqui converte e adiciona
          let plainObj = JSON.parse(JSON.stringify(requerimento));
          table.add(plainObj).then((doc) => {                                
              resolve();
          });
       });
    }

      /**
     * Atualiza um requerimento
     * @param aluno 
     * @param texto 
     */
    public updateRequerimento(aluno: Aluno, requerimento: Requerimento){

      // cria requerimento, repassando o id do aluno
      let doc = firebase.firestore().collection("requerimentos").doc("aluno_" + aluno.id)
      .collection("requerimentos")
      // localiza o documento especifico.
      .doc(requerimento.id);

      return new Promise( (resolve,reject)=>{

          // aqui converte e adiciona
          let plainObj = JSON.parse(JSON.stringify(requerimento));
          doc.update(plainObj).then((doc) => {                                
              resolve();
          });
       });
    }

    /**
     * exclui um requerimento
     * @param aluno 
     * @param requerimento 
     */
    public deleteRequerimento(aluno: Aluno,requerimento: Requerimento){

      // cria requerimento, repassando o id do aluno
      let doc = firebase.firestore().collection("requerimentos").doc("aluno_" + aluno.id)
      .collection("requerimentos")
      // localiza o documento especifico.
      .doc(requerimento.id);

      return new Promise( (resolve,reject)=>{

          
          doc.delete().then( ()=> {                                
              resolve();
          })
          .catch( () => {
              reject();
          });
       });
    }

    /** Carrega todos os requerimentos por Aluno */
    public loadRequerimentosPorAluno(aluno: Aluno) : Observable<any> {
      
      // aqui faz o filtro já na collection pelo aluno...
      let table = firebase.firestore().collection("requerimentos").doc("aluno_" + aluno.id).collection("requerimentos");

      return new Observable((observer) => {

          table.onSnapshot((querySnapshot) => {

              let requerimentos: Requerimentos = new Requerimentos();

              requerimentos.aluno = aluno;
              querySnapshot.forEach((doc) => {
                let data = doc.data();

                requerimentos.adicionarRequerimento(this.converteDataToRequerimento(doc.id,data));
              });
              observer.next(requerimentos);
          });
        });
    }

    
    public loadRequerimentoPorId(aluno: Aluno,id: string) : Promise<Requerimento> {
      
          // o filtro já aplica aqui, nenhuma outra consulta é necessária
          let docRef = firebase.firestore().collection("requerimentos")
            .doc("aluno_" + aluno.id).collection("requerimentos").doc(id);

        

          return new Promise((resolve,reject)=>{
            docRef.get()
            .then( (doc)=>{
              resolve(this.converteDataToRequerimento(doc.id,doc.data()));
            });

          });
        }

    private converteDataToRequerimento(id: string, data: any) : Requerimento{
      let requerimento = new Requerimento(data.numeroRequerimento, data.texto, data.status);
      requerimento.id = id;
      return requerimento;
    }

}
