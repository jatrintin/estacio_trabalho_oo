import {Injectable} from "@angular/core";
import { Aluno } from "src/Models/Aluno";

import * as firebase from 'firebase/app';
import 'firebase/firestore';

import { Observable } from "rxjs";

 

/**
 * Data access Object de Aluno
 */
@Injectable()
export class AlunoDAOservice {

    
    /**
     * Cria um aluno na base
     * @param aluno 
     */
    public postAluno(aluno): Promise<Aluno> {
        let table = firebase.firestore().collection('alunos');

        return new Promise( (resolve,reject)=>{
            
            let plainObj = JSON.parse(JSON.stringify(aluno));
            table.add(plainObj).then((doc) => {                                
                resolve();
            });
         });
    }

    public logar(email: string, senha: string): Promise<Aluno>{

        
        let table = firebase.firestore().collection('alunos');
        
        return new Promise( (resolve, reject)=>{

            table.onSnapshot((querySnapshot) => {
                
                // se não encontrou registros
                if (querySnapshot.size == 0){
                    reject();
                }
                else
                // se encontrou, tenta logar..
                querySnapshot.forEach((doc) => {
                  let data = doc.data();
                    
                  if (data.email == email && data.senha == senha){                  
                    let aluno = this.convertDataToAluno(doc.id,data)
                    resolve(aluno);
                  }
                  else 
                   reject(); // nao conseguiu logar    
                });              
              });
        });
    }


    // /**
    //  * Busca todos os alunos da base de dados
    //  */ 
    public getAlunos(): Observable<any>  {

        let table = firebase.firestore().collection('alunos');

        return new Observable((observer) => {

            table.onSnapshot((querySnapshot) => {
                let alunos = [];
                querySnapshot.forEach((doc) => {
                  let data = doc.data();
                    alunos.push(this.convertDataToAluno(doc.id,data));
                });
                observer.next(alunos);
            });
          });
        
    }

    // converte dados json no objeto aluno
    private convertDataToAluno(id: string, data: any) : Aluno{        
        let aluno = new Aluno(data.email, data.nome, data.senha, data.matricula, data.curso);
        aluno.id = id;
        return aluno;
    }
      
}