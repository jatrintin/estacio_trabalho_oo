import { Injectable } from '@angular/core';
import { Aluno } from 'src/Models/Aluno';

/**
 * Gerenciador de Sessão
 */
@Injectable({
  providedIn: 'root'
})
export class SessionService {

  alunoCache : Aluno;

  constructor() { 

  }

  public setAluno(aluno: Aluno){
    this.alunoCache = aluno;
  }


  public getAlunoLogado(): Aluno{
    return this.alunoCache;
  }
}
