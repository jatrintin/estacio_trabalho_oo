import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { RequerimentoCadastroComponent } from '../RequerimentoCadastro/RequerimentoCadastro.component';
import {AlunoDAOservice} from '../DAOS/AlunoDAO.service';
import {RequerimentosDAOService} from '../DAOS/RequerimentosDAO.service';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from '../Login/Login.component';
import { RequerimentosComponent } from '../Requerimentos/Requerimentos.component';
import { SessionService } from './Session.service';
import {DataTableModule} from "angular-6-datatable";


const appRoutes: Routes = [

   { path: 'login', component: LoginComponent },
   { path: 'requerimento/:id',  component: RequerimentoCadastroComponent },
   { path: 'requerimento',  component: RequerimentoCadastroComponent },
   { path: 'requerimentos', component: RequerimentosComponent },
];

@NgModule({
   declarations: [
      AppComponent,
      RequerimentoCadastroComponent,
      LoginComponent,
      RequerimentosComponent
   ],
   imports: [
      RouterModule.forRoot(appRoutes,
      ),
      BrowserModule,
      ReactiveFormsModule,
      DataTableModule
   ],
   providers: [
      AlunoDAOservice,
      RequerimentosDAOService,
      SessionService
   ],
   bootstrap: [
      AppComponent
   ]
})
export class AppModule { }
