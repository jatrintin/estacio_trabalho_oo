import { Component } from '@angular/core';
import * as firebase from 'firebase/app';
import 'firebase/firestore';
import { Router } from '@angular/router';

const config = {
  apiKey: 'AIzaSyDHnzwxs76fja6vvH4bBkzWjy1Ik5ck9ro',
  authDomain: 'estaciopioo.firebaseapp.com',
  databaseURL: 'https://estaciopioo.firebaseio.com',
  projectId: 'estaciopioo',
  storageBucket: 'estaciopioo.appspot.com',
};

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  

  constructor(private router: Router){
    router.navigate(['/login']);
  }

  ngOnInit() {
    firebase.initializeApp(config);
    // this.inicializacaoMock();
    


  }

  /** Inicializa base para testes */
  // inicializacaoMock(){
  //   // aluno mock
  //   let alunoMock = new Aluno("jose@email.com", "José da Silva", "123", "84899992", "Engenharia de software");

  //   // cria um aluno "mock"
  //   this.alunoDAOService.postAluno(alunoMock); 
  // }
  

}