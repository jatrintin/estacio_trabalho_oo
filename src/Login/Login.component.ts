import { Component, OnInit } from '@angular/core';
import { Aluno } from 'src/Models/Aluno';
import { FormControl, FormGroupDirective, FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { AlunoDAOservice } from 'src/DAOS/AlunoDAO.service';
import { Router } from '@angular/router';
import { SessionService } from 'src/app/Session.service';

@Component({
  selector: 'app-Login',
  templateUrl: './Login.component.html',
  styleUrls: ['./Login.component.css']
})
export class LoginComponent implements OnInit {
  
  LoginForm: FormGroup;

  constructor(private session: SessionService, private router: Router, private alunoDAOService: AlunoDAOservice, private formBuilder: FormBuilder) {

   }

  ngOnInit() {
      // login
      this.LoginForm = this.formBuilder.group({
        'email' : [null, Validators.required],
        'senha' : [null, Validators.required],
      });
  }

  onFormSubmit(data:NgForm) {

    this.alunoDAOService.logar(data['email'], data['senha'])
    
    .then((aluno)=> {
      alert("Login efetuado com sucesso!");
      
      this.session.setAluno(aluno);

      this.router.navigate(['/requerimentos']);
    })
    .catch(()=>{
      this.session.setAluno(null);
      alert("Login ou senha incorreta, por favor verique!");
    });

  }

}
